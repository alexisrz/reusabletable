import { Component } from '@angular/core';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private userservice: UserService){

  }

  tableData!: any[];
  tableColumns = [
    {field: "id", headers: "Sr No."},
    {field: "name", headers: "User Name."},
    {field: "email", headers: "Email"},
    {field: "phone", headers: "Phone"},
    {field: "website", headers: "Website"}
  ];

  ngOnInit() {
    this.loadGrid();
  }

  loadGrid() {
    this.userservice.getUser().subscribe((data: any) => {
      this.tableData = data;
    });
  }
}
